import React, { Component } from 'react';
import { View, Text, StyleSheet,TextInput, TouchableOpacity, Keyboard,Alert } from 'react-native'; 
import AsyncStorage from '@react-native-community/async-storage';
//const Config = ({ navigation }) => {
export default class Config extends Component {
     salvarConfig=async () => {
        try {
            await AsyncStorage.setItem('@NUMEROCONCLUI',this.state.conf_NUMEROCONCLUI);
            await AsyncStorage.setItem('@LINK',this.state.conf_LINK);
            Keyboard.dismiss();
            Alert.alert("Sucesso","Configuração gravada.");
        } catch(e){
            Alert.alert(e);
        }
    } 
    render(){
        return ( 
        <View  style={styles.productCountainer}>
            <Text style={styles.productTitle}>Nº Conclusão :</Text>
            <TextInput 
                style={styles.textBox}
                onChangeText = { (text) => this.setState({conf_NUMEROCONCLUI: text}) }
            >  </TextInput>
            <Text style={styles.productTitle}>Link Serviço:</Text>
            <TextInput 
                style={styles.textBox}
                onChangeText = { (text) => this.setState({conf_LINK: text}) }
            >  </TextInput>

            <TouchableOpacity  
                        style={styles.productButton} 
                        onPress={() => {
                            this.salvarConfig();
                        }}
                    > 
                        <Text  style={styles.productButtonText}>Salvar Configuração</Text>
                    </TouchableOpacity>
        </View>
        );
    }
};
//Config.navigationOptions = ({ navigation }) => ({
//    title:'Configurações',
//})

//export default Config; 

const styles = StyleSheet.create({
    container: { 
        flex:1,
        backgroundColor: "#fafafa"
    },
    list: {
        padding:20
    },
    productCountainer:{
        backgroundColor:'#FFF',
        borderWidth:1,
        borderColor:'#DDD',
        borderRadius:5,
        padding:20,
        marginBottom:20,
    },
    productTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333'
    },
    productDescription: {
        fontSize:16,
        color: '#999',
        marginTop:5,
        lineHeight:24
    },
    productButton: {
        height:42,
        borderRadius:5,
        borderWidth:2,
        borderColor: "#DA552F",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        marginTop:10
    },
    productButtonText: {
        fontSize:16,
        color: "#DA552F",
        fontWeight:"bold"
    },
    textBox:{
        backgroundColor:'#FFF',
        borderWidth:1,
        borderColor:'#DDD',
        borderRadius:5,
        padding:2,
       
    },
})