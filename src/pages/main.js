import React, { Component } from "react";
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from "react-native";
import { format, parseISO, differenceInMinutes} from "date-fns";
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
Icon.loadFont();
import qs from 'qs'
import api from '../services/api';

export default class Main extends Component { 
    static navigationOptions = ({ navigation }) => {

        return {
            title: 'MaxDelivery Produção',
            headerRight: () => (
                <TouchableOpacity  
                style={styles.productButton} 
                onPress={() => {
                    navigation.navigate('Config');
                }}
            > 
                <Icon name="cog" size={20} color="#FFF" />
                </TouchableOpacity>
            )
        }
    };
  
    state = {
        conf_NUMEROCONCLUI:0,
        productInfo: {}, 
        docs:[],
        page: 1
    }   
     componentDidMount() {
        this.loadProducts(); 
        var timer = setInterval(() => {
            this.loadProducts(); 
          },5000);

    }

    loadProducts = async (page = 1) => {  
        var conf_NUMEROCONCLUI='2';
        var conf_LINK='';
        try {
            conf_NUMEROCONCLUI=await AsyncStorage.getItem('@NUMEROCONCLUI');
            conf_LINK=await AsyncStorage.getItem('@LINK');
            if (conf_NUMEROCONCLUI===null){
                conf_NUMEROCONCLUI='2';
            }
        } catch(e){
            Alert.alert(e);
        }
        if (conf_LINK!=null && conf_LINK!='' && conf_NUMEROCONCLUI>0){
            const response = await api.get(conf_LINK+`/service.php?acao=listapedidosproducao`,{
                params: {
                    conf_NUMEROCONCLUI:conf_NUMEROCONCLUI
                } } ); 
            const { dados } = response.data; 
            const docs=dados;
            this.setState({
                conf_NUMEROCONCLUI:conf_NUMEROCONCLUI,  
                conf_LINK:conf_LINK,
                docs: docs,  //... this.state.docs,...docs
             //   productInfo, 
             //   page
            });
        }
       // const { docs, ...productInfo } = response.data;
      
    }

    loadMore = () => {
        const { page, productInfo } = this.state;
        if (page== productInfo.pages) return;
        const pageNumber=page+1;
        this.loadProducts(pageNumber);     
    };
    existeAdicional = ({ item }) => {
        if (item.ADICIONAL!=''){
            return  (
                <Text style={styles.productAdicional} >** {item.ADICIONAL} **</Text> 
            );
        } else {
            return  
        }
    }

    existeObs = ({ item }) => {
        if (item.OBS!=''){
            return  (
                <Text style={styles.productObs} >{item.OBS}</Text> 
            );
        } else {
            return  
        }
    }

    existeAdicionalG = ({ item }) => {
        if (item.ADICIONAL!=''){
            return  (
                <Text style={styles.productAdicionalG} >** {item.ADICIONAL} **</Text> 
                
            );
        } else {
            return  
        }
    }
    concluiProduto1 = async ({ item }) => {
        var dadosok = {item};
        dadosok['item']['acao']='concluiproduto1';
        var data =  qs.stringify(dadosok['item']);
        var array = [...this.state.docs]; 
        var index = array.indexOf(dadosok['item']);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({docs: array});
        }
        const response = await api.post(this.state.conf_LINK+`/service.php`, data);
       // console.log(ret);
    }
    concluiProduto2 = async ({ item }) => {
        var dadosok = {item};
        dadosok['item']['acao']='concluiproduto2';
        var data =  qs.stringify(dadosok['item']);
        var array = [...this.state.docs]; 
        var index = array.indexOf(dadosok['item']);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({docs: array});
        }
        const response = await api.post(this.state.conf_LINK+`/service.php`, data);
       // console.log(ret);
    }
    
    renderItem = ({ item }) => { 
        var DATAHORAATUAL=new Date();
        var dadositem=item;
        var differenceInMinutes = require('date-fns/differenceInMinutes');
        var diferenca = differenceInMinutes(
            DATAHORAATUAL,
            parseISO(dadositem.DATAHORA)
          );
          if (diferenca>30){
            var stylecx=styles.productCountainerDestaque;
          } else {
            var stylecx=styles.productCountainer;
          }
        //if (item.ADICIONAL!=''){ adicional=<View><Text style={styles.productAdicional} >** {item.ADICIONAL} **</Text></View> }
          if (this.state.conf_NUMEROCONCLUI==1){
            return (
            <View style={stylecx}>      
                <Text style={styles.productDescription}>Nº:{item.NUMMOVIMENTACAO}  </Text> 
                <Text style={styles.productTitle}>{item.PRODUTO}/{item.COMPLEMENTO}</Text> 
                <Text style={styles.productTitle}>Quantidade: ( {item.QUANT} )</Text> 
                <View>{this.existeAdicional({item})}</View>
                <View>{this.existeObs({item})}</View>
                <Text style={styles.productDescription}>{item.NOME}</Text>
                <Text style={styles.horamenos}>Hora Ped: {format(parseISO(item.DATAHORA),"HH:mm") }</Text> 
                <Text style={styles.productFalta}>Restantes: ( {item.RESTANTES} ).</Text> 
                <TouchableOpacity  
                    style={styles.productButton} 
                    onPress={() => {
                        this.concluiProduto1({item});
                    }}
                > 
                    <Text  style={styles.productButtonTextG}>CONCLUIR</Text>
                </TouchableOpacity>

            </View>
            );
        }

        if (this.state.conf_NUMEROCONCLUI==2){
            return (
            <View style={stylecx}>      
                <Text style={styles.productDescription}>Nº:{item.NUMMOVIMENTACAO}  </Text> 
                <Text style={styles.productTitle}>{item.PRODUTO}/{item.COMPLEMENTO}</Text> 
                <Text style={styles.productTitle}>Quantidade: ( {item.QUANT} )</Text> 
                <View>{this.existeAdicional({item})}</View>
                <View>{this.existeObs({item})}</View>
                <Text style={styles.productDescription}>{item.NOME}</Text>
                <Text style={styles.horamenos}>Hora Ped.: {format(parseISO(item.DATAHORA),"HH:mm") }</Text> 
                <TouchableOpacity  
                    style={styles.productButton} 
                    onPress={() => {
                        this.concluiProduto2({item});
                    }}
                > 
                    <Text  style={styles.productButtonText}>CONCLUIR</Text>
                </TouchableOpacity>

            </View>
            );
        }
    }


    render(){   
            return ( 
                
                <View style={styles.container}>
                <FlatList
                        contentContainerStyle={styles.list}
                        data={this.state.docs}
                        keyExtractor={item => item.ID_PRODUTO} 
                        renderItem={this.renderItem}
                        //onEndReached={this.loadMore}
                        onEndReachedThreshold={0.1}
                    />
                </View>
            );

            

    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: "#fafafa"
    },
    list: {
        padding:20
    },
    productCountainer:{
        backgroundColor:'#FFF',
        borderWidth:1,
        borderColor:'#DDD',
        borderRadius:5,
        padding:20,
        marginBottom:20,
    },
    productCountainerDestaque:{
        backgroundColor:'#FFF',
        borderWidth:3,
        borderColor:'#cc3600',
        borderRadius:5,
        padding:20,
        marginBottom:20,
        elevation: 6,
       
    },
    productTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333'
    },
    productAdicional:{
        fontSize: 21,
        fontWeight: 'bold',
        color: "#fc6203"
    },
    productObs:{
        fontSize: 21,
        fontWeight: 'bold',
        color: "##ba000c"
    },
    productDescription: {
        fontSize:16,
        color: '#999',
        marginTop:5,
        lineHeight:24
    },
    horamenos: {
        fontSize:18,
        color: '#000',
        marginTop:5,
        lineHeight:24,
        fontWeight: 'bold'
    },
    productButton: {
        height:42,
        borderRadius:5,
        borderWidth:2,
        borderColor: "#DA552F",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        marginTop:10
    },
    productFalta: {
        fontSize:16,
        color: '#999',
        marginTop:5,
        lineHeight:25, 
        textAlign: "right"
    },
    productButtonText: {
        fontSize:16,
        color: "#DA552F",
        fontWeight:"bold"
    },


    // G
    
    productTitleG:{
        fontSize: 58,
        fontWeight: 'bold',
        color: '#333'
    },
    productAdicionalG:{
        fontSize:51,
        fontWeight: 'bold',
        color: "#fc6203"
    },
    productDescriptionG: {
        fontSize:46,
        color: '#999',
        marginTop:5,
        lineHeight:54
    },
 
    horamenosG: {
        fontSize:38,
        color: '#000',
        marginTop:5,
        lineHeight:54,
        fontWeight: 'bold'
    },
    productButtonG: {
        height:62,
        borderRadius:5,
        borderWidth:2,
        borderColor: "#DA552F",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        marginTop:10
    },
    productButtonTextG: {
        fontSize:16,
        color: "#DA552F",
        fontWeight:"bold"
    }


})