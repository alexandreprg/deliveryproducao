import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Main from "./pages/main";
import Config from "./pages/config";

const AppNavigator = createStackNavigator({
   Main,
   Config 
 },{
    defaultNavigationOptions:{
            headerStyle: {
                backgroundColor: "#DA552F" 
            },
            headerTintColor:"#FFF"
            
        }
    }
  );
  
  export default createAppContainer(AppNavigator);




